use strict;
use warnings;

#use Proc::Daemon;

use Test::More;
use Test::Command::Simple;

my $NAME = 'Konclude';
my $CMD  = $ENV{'KONCLUDE'} || 'Konclude';

run_ok $CMD;
like stdout, qr/ Usage: '$NAME'/, 'bare command, stdout';
cmp_ok stderr, 'eq', '', 'bare command, stderr';

run_ok $CMD, qw(-h);
like stdout, qr/ Usage: '$NAME'/, 'version, stdout';
cmp_ok stderr, 'eq', '', 'version, stderr';

run_ok $CMD, qw(owllinkfile -i Tests/test-request.xml -o test-response.xml);
like stdout, qr[Realizing ontology 'http://www.konclude.com/test'], 'test, stdout';
cmp_ok stderr, 'eq', '', 'test, stderr';

run_ok $CMD, qw(owllinkfile -c Configs/default-config.xml -i Tests/test-request.xml -o test-response-cfg.xml);
like stdout, qr[Realizing ontology 'http://www.konclude.com/test'], 'test with config, stdout';
cmp_ok stderr, 'eq', '', 'test with config, stderr';

# FIXME
#my $daemon_owl = Proc::Daemon->new({
#	exec_command => "$Konclude owllinkserver -w 2 -c Configs/default-config.xml -p 8080",
#});
##check stdout and stderr
##POST < Tests/test-request.xml http://localhost:8080
## shut down daemon

# TODO: check sanity of output files

run_ok $CMD, qw(satisfiability -i Tests/galen.owl.xml -x), 'http://ex.test/galen#ExternalIliacVein';
like stdout, qr[Class 'http://ex.test/galen#ExternalIliacVein' for ontology 'Tests/galen.owl.xml' is satisfiable.], 'satisfiability, stdout';
cmp_ok stderr, 'eq', '', 'satisfiability, stderr';

run_ok $CMD, qw(classification -w AUTO -i Tests/roberts-family-full-D.owl.xml -o test-response-class.xml);
like stdout, qr[Finished class classification in \d+ ms for ontology 'http://konclude.com/test/kb'.], 'classification, stdout';
cmp_ok stderr, 'eq', '', 'classification, stderr';

# TODO: check sanity of output files

run_ok $CMD, qw(realization -w AUTO -i Tests/roberts-family-full-D.owl.xml -o test-response-real.xml);
like stdout, qr[Finished class classification in \d+ ms for ontology 'http://konclude.com/test/kb'.], 'realization, stdout';
cmp_ok stderr, 'eq', '', 'realization, stderr';

# TODO: check sanity of output files

run_ok $CMD, qw(sparqlfile -s Tests/lubm-univ-bench-sparql-load-and-query-test.sparql -o Tests/query-answers.xml -c Configs/querying-config.xml);
like stdout, qr[Determined 208 answers for complex ABox query with 1 answer variables], 'test SPARQL query, stdout';
cmp_ok stderr, 'eq', '', 'test SPARQL query, stderr';

run_ok $CMD, qw(sparqlfile -s Tests/lubm-univ-bench-sparql-load-and-complex-query-rasqal-test.sparql -o Tests/query-answers.xml -c Configs/querying-config.xml);
like stdout, qr[Determined 208 answers for complex ABox query with 1 answer variables], 'test SPARQL query, stdout';
cmp_ok stderr, 'eq', '', 'test SPARQL query, stderr';

# TODO: check sanity of output files

# FIXME
#my $daemon_sparql = Proc::Daemon->new({
#	exec_command => "$CMD sparqlserver -w 2 -c Configs/querying-config.xml -p 8080",
#});
##check stdout and stderr
## redo SPARQL query against daemon (not directly to file)
## shut down daemon

# TODO: check sanity of output files

done_testing;
